import {Injectable} from '@angular/core';
import {Album} from '../../Album';
import 'rxjs/add/operator/map';

/**
Encargado de almacenar y eliminar los productos que se agregan a
la lista de produuctos comprados pro el usuario
**/
@Injectable()
export class ShopcartService{
   albums:Album[] = [];

   add(album:Album){
     this.albums.push(album);
   }

   remove(album:Album){
     var index = this.albums.indexOf(album);
     if (index != -1) {
       this.albums.splice(index, 1);
    }
   }

   getAlbum() {
   	 return this.albums;
   }
}
