import { Component } from '@angular/core';
import {NavbarComponent} from './components/navbar/navbar.component';
import {ROUTER_DIRECTIVES} from '@angular/router';
import {HomeComponent} from './components/home/home.component';
import {CartComponent} from './components/cart/cart.component';
import {AboutComponent} from './components/about/about.component';
import {AlbumComponent} from './components/album/album.component';
import {ArtistComponent} from './components/artist/artist.component';
import {Notifications} from './components/notifications/notification-component';
import {HTTP_PROVIDERS} from '@angular/http';
import {SpotifyService} from './services/spotify.service';
import {NotificationsService} from './components/notifications/notification-service';
import {ShopcartService} from './services/shopcart.service';

@Component({
    moduleId:module.id,
    selector: 'my-app',
    templateUrl: 'app.component.html',
    directives:[ROUTER_DIRECTIVES, NavbarComponent, Notifications],
    precompile:[HomeComponent, AboutComponent, CartComponent],
    providers:[HTTP_PROVIDERS, SpotifyService, ShopcartService, NotificationsService]
})
export class AppComponent { }
