import {provideRouter, RouterConfig} from '@angular/router';
import {HomeComponent} from './components/home/home.component';
import {AboutComponent} from './components/about/about.component';
import {CartComponent} from './components/cart/cart.component';
import {ArtistComponent} from './components/artist/artist.component';
import {AlbumComponent} from './components/album/album.component';

const routes: RouterConfig = [
    {path:'', component:HomeComponent},
    {path:'about', component: AboutComponent},
    {path:'cart', component: CartComponent},
    {path:'artist/:id', component:ArtistComponent},
    {path:'album/:id', component:AlbumComponent}
];

export const appRouterProviders = [
    provideRouter(routes)
];
