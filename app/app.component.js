"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var navbar_component_1 = require('./components/navbar/navbar.component');
var router_1 = require('@angular/router');
var home_component_1 = require('./components/home/home.component');
var cart_component_1 = require('./components/cart/cart.component');
var about_component_1 = require('./components/about/about.component');
var notification_component_1 = require('./components/notifications/notification-component');
var http_1 = require('@angular/http');
var spotify_service_1 = require('./services/spotify.service');
var notification_service_1 = require('./components/notifications/notification-service');
var shopcart_service_1 = require('./services/shopcart.service');
var AppComponent = (function () {
    function AppComponent() {
    }
    AppComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'my-app',
            templateUrl: 'app.component.html',
            directives: [router_1.ROUTER_DIRECTIVES, navbar_component_1.NavbarComponent, notification_component_1.Notifications],
            precompile: [home_component_1.HomeComponent, about_component_1.AboutComponent, cart_component_1.CartComponent],
            providers: [http_1.HTTP_PROVIDERS, spotify_service_1.SpotifyService, shopcart_service_1.ShopcartService, notification_service_1.NotificationsService]
        }), 
        __metadata('design:paramtypes', [])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map