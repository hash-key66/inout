import { Component, OnInit } from '@angular/core';
import {ShopcartService} from'../../services/shopcart.service';
import {Album} from '../../../Album';
import {NotificationsService} from "../notifications/notification-service";
import {Notification} from "../notifications/notifications-model";

/**
  Carrito de compra
**/
@Component({
    moduleId:module.id,
    selector: 'about',
    templateUrl: 'cart.component.html'
})
export class CartComponent implements  OnInit{
	albums:Album[];

	constructor(private _shopcartService:ShopcartService,
              private _notes: NotificationsService){
    }

    ngOnInit(){
    	this.albums = this._shopcartService.getAlbum();
    }

    removeShopCart(album:Album){
        this._shopcartService.remove(album);
        this._notes.add(new Notification('info', 'El albúm '+ album.name +' se a eliminado del carrito de compras'));
    }
}
