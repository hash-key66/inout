import { Component, OnInit } from '@angular/core';
import {ROUTER_DIRECTIVES} from '@angular/router';
import {SpotifyService} from '../../services/spotify.service';
import {Artist} from '../../../Artist';
import {Album} from '../../../Album';
import {ActivatedRoute} from '@angular/router';
import {ShopcartService} from'../../services/shopcart.service';
import {NotificationsService} from "../notifications/notification-service";
import {Notification} from "../notifications/notifications-model";

/**
Componente que lista a los artistas por el id
*/
@Component({
    moduleId:module.id,
    selector: 'artist',
    templateUrl: 'artist.component.html',
    directives: [ROUTER_DIRECTIVES]
})
export class ArtistComponent implements OnInit{
    id:string;
    artist: Artist[];
    albums:Album[];

    constructor(private _spotifyService:SpotifyService,
                private _route:ActivatedRoute,
                private _shopcartService:ShopcartService,
                private _notes: NotificationsService){
    }

   addShopCart(album:Album){
       this._shopcartService.add(album);
       this._notes.add(new Notification('success', 'El albúm '+ album.name +' agregado al carrito de compras'));
   }

    ngOnInit(){
        this._route.params
            .map(params => params['id'])
            .subscribe((id) => {
                this._spotifyService.getArtist(id)
                    .subscribe(artist => {
                        this.artist = artist;
                    })

                this._spotifyService.getAlbums(id)
                    .subscribe(albums => {
                        this.albums = albums.items;
                    })
            })
    }
}
