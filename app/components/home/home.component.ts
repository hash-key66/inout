import { Component, OnInit} from '@angular/core';
import {SpotifyService} from '../../services/spotify.service';
import {ShopcartService} from'../../services/shopcart.service';
import {NotificationsService} from "../notifications/notification-service";
import {Artist} from '../../../Artist';
import {Notification} from "../notifications/notifications-model";
import {ROUTER_DIRECTIVES} from '@angular/router';
import {Album} from '../../../Album';

/**
 Vista principal
**/
@Component({
    moduleId:module.id,
    selector: 'home',
    templateUrl: 'home.component.html',
    providers:[SpotifyService],
    directives:[ROUTER_DIRECTIVES]
})
export class HomeComponent implements OnInit {
  searchStr:string;
  searchRes: Artist[];

  constructor(private _spotifyService:SpotifyService,
              private _shopcartService:ShopcartService,
              private _notes: NotificationsService){
  }

  searchMusic(){
      this._spotifyService.searchMusic(this.searchStr)
          .subscribe(res => {
              this.searchRes = res.artists.items;
          })
  }

  addShopCart(album:Album){
    this._shopcartService.add(album);
    this._notes.add(new Notification('success', 'El albúm '+ album.name +' agregado al carrito de compras'));
  }

  ngOnInit(){
    this._spotifyService.searchMusic(this.ramdonSearch())
        .subscribe(res => {
            this.searchRes = res.artists.items;
        })
    }

    ramdonSearch(){
      var myArray = ['af', 'andres', 'un', 't', 'ra', 'aero', 'radio'];
      return myArray[Math.floor(Math.random() * myArray.length)];
    }
}
