import { Component, OnInit } from '@angular/core';
import {ROUTER_DIRECTIVES} from '@angular/router';
import {SpotifyService} from '../../services/spotify.service';
import {Artist} from '../../../Artist';
import {Album} from '../../../Album';
import {ActivatedRoute} from '@angular/router';
import {ShopcartService} from'../../services/shopcart.service';
import {NotificationsService} from "../notifications/notification-service";
import {Notification} from "../notifications/notifications-model";

/**
  Componente con la funcionalidad de listas los albumes
  por artista
**/
@Component({
    moduleId:module.id,
    selector: 'album',
    templateUrl: 'album.component.html',
    directives:[ROUTER_DIRECTIVES]
})
export class AlbumComponent {
    id:string;
    album:Album[];

    constructor(private _spotifyService:SpotifyService,
                private _route:ActivatedRoute,
                private _shopcartService:ShopcartService,
                private _notes: NotificationsService){
    }

    addShopCart(name:Album){
       this._shopcartService.add(name);
       this._notes.add(new Notification('success', 'El albúm '+ album.name +' agregado al carrito de compras'));
    }

    ngOnInit(){
        this._route.params
            .map(params => params['id'])
            .subscribe((id) => {
                this._spotifyService.getAlbum(id)
                    .subscribe(album => {
                        this.album = album;
                    })
            })
    }
}
