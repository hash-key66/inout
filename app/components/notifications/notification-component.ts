import {Component} from "@angular/core";
import {Notification} from "./notifications-model";
import {NotificationsService} from "./notification-service";
import {Observable} from "rxjs/Rx";

@Component({
    selector: 'notifications',
    template:`
    <div class="notifications">
      <div (click)="hide(note)" class="alert alert-{{note.type}}"
               *ngFor="let note of _notes">
           {{ note.message }}
       </div>
    </div>
    `
})
export class Notifications {
    private _notes: Notification[];

    constructor(private _notifications: NotificationsService) {
        this._notes = new Array<Notification>();

        _notifications.noteAdded.subscribe(note => {
            this._notes.push(note);

            setTimeout(() => { this.hide.bind(this)(note) }, 3000);
        });
    }

    private hide(note:any) {
        let index = this._notes.indexOf(note);

        if (index >= 0) {
            this._notes.splice(index, 1);
        }
    }
}
